SyToGen Suite
==========================================
#### Gianmarco Piccinno, Serena Manara, Christopher D. Johnston, Nicola Segata.

The SyToGen Suite is composed of four different modules. The modules are:
---
  1. Plasmid assembler
  2. Codon usage counter
  3. Target motifs' evaluation
  4. SynPlEst

### PLASMID ASSEMBLER
#### Input:
* Multifasta of subsequences composing the plasmid (if the subsequence is genetically relevant, it should be annotated).

#### Output:
* Annotated plasmid in gbk and multifasta formats

### CODON USAGE
#### Input:
* Annotated genome (both multifasta or gbk)
* Codon table specification

#### Output:
* Codon usage table

### TARGET MOTIF EVALUATION
#### Input:
* Target motifs
* REBASE output table
* CRISPR information

#### Output:
* Non-overlapping motifs (union of overlapping motifs) with possible epigenetic modification (yes/no and position on which the modification must occur).  
The list of motifs to be generalized and modified depends on the user according to the output information of this module.

### SYNGENIC PLASMID ESTIMATION (SynPlEst)
#### Input:
* Annotated plasmid in gbk or multifasta
* Codon table specification
* Evaluated target motifs
* Output folder

#### Output:
* List of best k syngenic plasmids with a common report. The value of k depends on the number of plasmids that have similar statistics
