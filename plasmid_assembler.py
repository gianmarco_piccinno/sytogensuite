#!/usr/bin/env python3

import argparse as ap
import re

from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation

"""
Input: Multifasta of subsequences - if a sequence is genetically relevant (CDS, primer, etc.) indicate the annotation in the header

Output: Annotated plasmid in gbk format
"""

def plasmid_assembler(data = 'example.fna'):

    seq = Seq('', IUPAC.unambiguous_dna)

    features = []

    with open(data, "rU") as handle:
        for record in SeqIO.parse(handle, 'fasta'):
            #print(record.id)
            if 'CDS' in record.id:
                if '-' in record.id:
                    features.append(SeqFeature(FeatureLocation(len(seq), len(seq)+len(record.seq)), type="CDS", strand=-1))#, id=record.id.split(':')[1])
                else:
                    features.append(SeqFeature(FeatureLocation(len(seq), len(seq)+len(record.seq)), type="CDS", strand=+1)) #, id=record.id.split(':')[1])
            print(record.id)
            seq = seq + record.seq
    # >CDS[+/-]: name_of_the_CDS
    print(type(seq))
    seq.alphabet = IUPAC.unambiguous_dna
    seq = SeqRecord(seq, features = features, name='')

    return seq


if __name__ == '__main__':

    parser = ap.ArgumentParser(description=
    'This script builds a plasmid object from a multifasta file.\n'
    'The SeqRecord object is then written in a gbk file.\n'
    'Usage:\n'
    'plasmid_assembler.py -i example.fa -o gbk_example\n',
    formatter_class=ap.RawTextHelpFormatter)

    parser.add_argument('-i','--input', help='The path to the input file',required=True)
    parser.add_argument('-o','--output', help='The path to the output directory', required=True)
    args = vars(parser.parse_args())

    seq = plasmid_assembler(data=args['input'])

    print(seq); print('\n'); print(seq.features); print('\n')#; print(seq.id)

    with open(args['output'], "w") as outf:
        SeqIO.write(seq, outf, 'genbank')
